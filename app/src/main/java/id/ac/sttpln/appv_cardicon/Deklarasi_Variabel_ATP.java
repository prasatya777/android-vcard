package id.ac.sttpln.appv_cardicon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Deklarasi_Variabel_ATP {
    @Expose
    @SerializedName("email_pegawai") private String email_pegawai_atp;
    @Expose
    @SerializedName("nama_pegawai") private String nama_pegawai_atp;
    @Expose
    @SerializedName("bagian_pegawai") private String bagian_pegawai_atp;
    @Expose
    @SerializedName("telepon_pegawai") private String telepon_pegawai_atp;
    @Expose
    @SerializedName("alamat_pegawai") private String alamat_pegawai_atp;
    @Expose
    @SerializedName("password_pegawai") private String password_pegawai_atp;
    @Expose
    @SerializedName("success") private Boolean success_atp;
    @Expose
    @SerializedName("message") private String message_atp;

//   Variabel Session
    @Expose
    @SerializedName("s_email_pegawai") private String s_email_pegawai_atp;

    public String getEmail_pegawai_atp() {
        return email_pegawai_atp;
    }

    public void setEmail_pegawai_atp(String email_pegawai_atp) {
        this.email_pegawai_atp = email_pegawai_atp;
    }

    public String getNama_pegawai_atp() {
        return nama_pegawai_atp;
    }

    public void setNama_pegawai_atp(String nama_pegawai_atp) {
        this.nama_pegawai_atp = nama_pegawai_atp;
    }

    public String getBagian_pegawai_atp() {
        return bagian_pegawai_atp;
    }

    public void setBagian_pegawai_atp(String bagian_pegawai_atp) {
        this.bagian_pegawai_atp = bagian_pegawai_atp;
    }

    public String getTelepon_pegawai_atp() {
        return telepon_pegawai_atp;
    }

    public void setTelepon_pegawai_atp(String telepon_pegawai_atp) {
        this.telepon_pegawai_atp = telepon_pegawai_atp;
    }

    public String getAlamat_pegawai_atp() {
        return alamat_pegawai_atp;
    }

    public void setAlamat_pegawai_atp(String alamat_pegawai_atp) {
        this.alamat_pegawai_atp = alamat_pegawai_atp;
    }

    public String getPassword_pegawai_atp() {
        return password_pegawai_atp;
    }

    public void setPassword_pegawai_atp(String password_pegawai_atp) {
        this.password_pegawai_atp = password_pegawai_atp;
    }

    public Boolean getSuccess_atp() {
        return success_atp;
    }

    public void setSuccess_atp(Boolean success_atp) {
        this.success_atp = success_atp;
    }

    public String getMessage_atp() {
        return message_atp;
    }

    public void setMessage_atp(String message_atp) {
        this.message_atp = message_atp;
    }

    public String getS_email_pegawai_atp() {
        return s_email_pegawai_atp;
    }

    public void setS_email_pegawai_atp(String s_email_pegawai_atp) {
        this.s_email_pegawai_atp = s_email_pegawai_atp;
    }
}
