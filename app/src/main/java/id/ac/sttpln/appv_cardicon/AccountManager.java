package id.ac.sttpln.appv_cardicon;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountManager extends AppCompatActivity {

    EditText txtUsername_jv, txtNewPassword_jv, txtReNewPassword_jv;
    Button btnUpdateAM_jv;
    String txtNewPassword_validasi, txtReNewPassword_validasi;

    String email_to_all;
    ProgressDialog progressDialog;
    ApiInterface apiInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_manager);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu......");

        Bundle email_r = getIntent().getExtras();
        email_to_all = email_r.getString("email");

        txtUsername_jv = (EditText)findViewById(R.id.txtUsername);
        txtNewPassword_jv = (EditText)findViewById(R.id.txtNewPassword);
        txtReNewPassword_jv = (EditText)findViewById(R.id.txtReNewPassword);
        btnUpdateAM_jv = (Button)findViewById(R.id.btnUpdateAM);

        txtUsername_jv.setText(email_to_all);

        btnUpdateAM_jv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear_seterror();
                validasi_AccountManager();
            }
        });

    }

    public void validasi_AccountManager(){
        txtNewPassword_validasi = txtNewPassword_jv.getText().toString();
        txtReNewPassword_validasi = txtReNewPassword_jv.getText().toString();

        if (TextUtils.isEmpty(txtNewPassword_validasi))
        {
            txtNewPassword_jv.setError("Maaf Password Baru Tidak Boleh Kosong");
            txtNewPassword_jv.requestFocus();
        }
        else if (TextUtils.isEmpty(txtReNewPassword_validasi))
        {
            txtReNewPassword_jv.setError("Maaf Ketikkan Ulang Password Baru");
            txtReNewPassword_jv.requestFocus();
        }
        else if (!txtNewPassword_validasi.equals(txtReNewPassword_validasi))
        {
            AlertDialog.Builder builder= new AlertDialog.Builder(AccountManager.this);
            builder.setMessage("Password Yang Dimasukkan Tidak Sesuai")
                    .setNegativeButton("Retry",null).create().show();
            txtNewPassword_jv.setText("");
            txtReNewPassword_jv.setText("");
            txtNewPassword_jv.requestFocus();
        }
        else
        {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            set_updatepswd_vcard();
                            txtNewPassword_jv.setText("");
                            txtReNewPassword_jv.setText("");
                            txtNewPassword_jv.requestFocus();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(AccountManager.this);
            builder.setMessage("Apakah Anda Yakin Update Password?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
    }

    public void clear_seterror()
    {
        txtNewPassword_jv.setError(null);
        txtReNewPassword_jv.setError(null);
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(AccountManager.this);
        builder.setMessage("Apakah Anda Yakin Menutup Aplikasi Ini?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_vcard) {
            Intent i = new Intent(AccountManager.this,VCard.class);

            Bundle email_s = new Bundle();
            email_s.putString("email",email_to_all);
            i.putExtras(email_s);

            startActivity(i);
            finish();
        }
        else if (id == R.id.action_biodata) {
            Intent i = new Intent(AccountManager.this,Biodata.class);

            Bundle email_s = new Bundle();
            email_s.putString("email",email_to_all);
            i.putExtras(email_s);

            startActivity(i);
            finish();
        }
        else if (id == R.id.action_account_manager) {
            Intent i = new Intent(AccountManager.this,AccountManager.class);

            Bundle email_s = new Bundle();
            email_s.putString("email",email_to_all);
            i.putExtras(email_s);

            startActivity(i);
            finish();
        }
        else if (id == R.id.action_logout) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            Toast.makeText(getApplicationContext(),"Logout Sukses",Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(AccountManager.this,MainActivity.class);
                            startActivity(i);
                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(AccountManager.this);
            builder.setMessage("Apakah Anda Yakin Keluar?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    public void set_updatepswd_vcard(){
        String password_pegawai = txtNewPassword_jv.getText().toString().trim();
        updatepswd_vcard(email_to_all, password_pegawai);
    }

    public void updatepswd_vcard(final String email_pegawai_atp, final String password_pegawai_atp) {
        progressDialog.show();
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Deklarasi_Variabel_ATP> call = apiInterface.updatepswdVcard(email_pegawai_atp, password_pegawai_atp);
        call.enqueue(new Callback<Deklarasi_Variabel_ATP>() {
            @Override
            public void onResponse(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Response<Deklarasi_Variabel_ATP> response) {
                progressDialog.dismiss();
                if(response.isSuccessful() && response.body() != null)
                {
                    Boolean success = response.body().getSuccess_atp();
                    if (success)
                    {
                        Toast.makeText(AccountManager.this,response.body().getMessage_atp(),Toast.LENGTH_LONG).show();
                        email_to_all = response.body().getS_email_pegawai_atp();
                        txtUsername_jv.setText(email_to_all);
                    }
                    else
                    {
                        Toast.makeText(AccountManager.this,response.body().getMessage_atp(),Toast.LENGTH_LONG).show();
                        Intent i = new Intent(AccountManager.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(AccountManager.this,t.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                Intent i = new Intent(AccountManager.this,MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

}
