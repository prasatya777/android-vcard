package id.ac.sttpln.appv_cardicon;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("in_vcard.php")
    Call<Deklarasi_Variabel_ATP> insertVcard(
        @Field("email_pegawai") String email_pegawai_atp,
        @Field("nama_pegawai") String nama_pegawai_atp,
        @Field("bagian_pegawai") String bagian_pegawai_atp,
        @Field("telepon_pegawai") String telepon_pegawai_atp,
        @Field("alamat_pegawai") String alamat_pegawai_atp,
        @Field("password_pegawai") String password_pegawai_atp
    );

    @FormUrlEncoded
    @POST("val_login_vcard.php")
    Call<Deklarasi_Variabel_ATP> valloginVcard(
            @Field("email_pegawai") String email_pegawai_atp,
            @Field("password_pegawai") String password_pegawai_atp
    );

    @FormUrlEncoded
    @POST("vw_t_vcard.php")
    Call<Deklarasi_Variabel_ATP> viewVcard(
            @Field("email_pegawai") String email_pegawai_atp
    );


    @FormUrlEncoded
    @POST("up_vcard.php")
    Call<Deklarasi_Variabel_ATP> updatebiodataVcard(
            @Field("s_email_pegawai") String s_email_pegawai_atp,
            @Field("email_pegawai") String email_pegawai_atp,
            @Field("nama_pegawai") String nama_pegawai_atp,
            @Field("bagian_pegawai") String bagian_pegawai_atp,
            @Field("telepon_pegawai") String telepon_pegawai_atp,
            @Field("alamat_pegawai") String alamat_pegawai_atp
    );

    @FormUrlEncoded
    @POST("up_pswd_vcard.php")
    Call<Deklarasi_Variabel_ATP> updatepswdVcard(
            @Field("s_email_pegawai") String s_email_pegawai_atp,
            @Field("password_pegawai") String password_pegawai_atp
    );

}
