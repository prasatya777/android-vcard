package id.ac.sttpln.appv_cardicon;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {

    EditText txtEmail_jv, txtNamaPegawai_jv, txtBagianPekerjaan_jv, txtTelepon_jv, txtAlamat_jv, txtNewPassword_jv, txtReNewPassword_jv;
    Button btnUpdate_jv;
    String txtEmail_validasi, txtNamaPegawai_validasi, txtBagianPekerjaan_validasi, txtTelepon_validasi, txtAlamat_validasi, txtNewPassword_validasi, txtReNewPassword_validasi;
    ProgressDialog progressDialog;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtEmail_jv = (EditText)findViewById(R.id.txtEmailRegister);
        txtNamaPegawai_jv = (EditText)findViewById(R.id.txtNamaPegawaiRegister);
        txtBagianPekerjaan_jv = (EditText)findViewById(R.id.txtBagianPekerjaanRegister);
        txtTelepon_jv = (EditText)findViewById(R.id.txtTeleponRegister);
        txtAlamat_jv = (EditText)findViewById(R.id.txtAlamatRegister);
        txtNewPassword_jv = (EditText)findViewById(R.id.txtNewPasswordRegister);
        txtReNewPassword_jv = (EditText)findViewById(R.id.txtReNewPasswordRegister);
        btnUpdate_jv = (Button)findViewById(R.id.btnUpdateRegister);

//      Scroll Text Alamat dalam Scroll View
        txtAlamat_jv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (txtAlamat_jv.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });

//       Buton Update
        btnUpdate_jv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear_seterror();
                txtEmail_validasi = txtEmail_jv.getText().toString();
                if (TextUtils.isEmpty(txtEmail_validasi))
                {
                    txtEmail_jv.setError("Maaf Email Tidak Boleh Kosong");
                    txtEmail_jv.requestFocus();
                }
                else if (isValidEmail(txtEmail_jv.getText().toString()))
                {
                    validasi_biodata();
                } else {
                    txtEmail_jv.setError("Format Email Salah");
                    txtEmail_jv.requestFocus();
                }

            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu......");
    }


    public static boolean isValidEmail(String email) {
        boolean validate;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String emailPattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";

        if (email.matches(emailPattern)) {
            validate = true;
        } else if (email.matches(emailPattern2)) {
            validate = true;
        } else {
            validate = false;
        }

        return validate;
    }

    public void validasi_biodata(){
        txtNamaPegawai_validasi = txtNamaPegawai_jv.getText().toString();
        txtBagianPekerjaan_validasi = txtBagianPekerjaan_jv.getText().toString();
        txtTelepon_validasi = txtTelepon_jv.getText().toString();
        txtAlamat_validasi = txtAlamat_jv.getText().toString();
        txtNewPassword_validasi = txtNewPassword_jv.getText().toString();
        txtReNewPassword_validasi = txtReNewPassword_jv.getText().toString();


        if (TextUtils.isEmpty(txtNamaPegawai_validasi))
        {
            txtNamaPegawai_jv.setError("Maaf Nama Pegawai Tidak Boleh Kosong");
            txtNamaPegawai_jv.requestFocus();
        }
        else if (TextUtils.isEmpty(txtBagianPekerjaan_validasi))
        {
            txtBagianPekerjaan_jv.setError("Maaf Bagian Pekerjaan Tidak Boleh Kosong");
            txtBagianPekerjaan_jv.requestFocus();
        }
        else if (TextUtils.isEmpty(txtTelepon_validasi))
        {
            txtTelepon_jv.setError("Maaf Telepon Tidak Boleh Kosong");
            txtTelepon_jv.requestFocus();
        }
        else if (TextUtils.isEmpty(txtAlamat_validasi))
        {
            txtAlamat_jv.setError("Maaf Alamat Tidak Boleh Kosong");
            txtAlamat_jv.requestFocus();
        }
        else if (TextUtils.isEmpty(txtNewPassword_validasi))
        {
            txtNewPassword_jv.setError("Maaf Password Tidak Boleh Kosong");
            txtNewPassword_jv.requestFocus();
        }
        else if (TextUtils.isEmpty(txtReNewPassword_validasi))
        {
            txtReNewPassword_jv.setError("Maaf Ketikkan Ulang Password");
            txtReNewPassword_jv.requestFocus();
        }
        else if (!txtNewPassword_validasi.equals(txtReNewPassword_validasi))
        {
            AlertDialog.Builder builder= new AlertDialog.Builder(Register.this);
            builder.setMessage("Password Yang Dimasukkan Tidak Sesuai")
                    .setNegativeButton("Retry",null).create().show();
            txtNewPassword_jv.setText("");
            txtReNewPassword_jv.setText("");
            txtNewPassword_jv.requestFocus();
        }
        else
        {
            save_register();
            after_add();
        }
    }
    public void after_add(){
        txtEmail_jv.setText("");
        txtNamaPegawai_jv.setText("");
        txtBagianPekerjaan_jv.setText("");
        txtTelepon_jv.setText("");
        txtAlamat_jv.setText("");
        txtNewPassword_jv.setText("");
        txtReNewPassword_jv.setText("");
        txtEmail_jv.requestFocus();
    }

    public void clear_seterror()
    {
        txtEmail_jv.setError(null);
        txtNamaPegawai_jv.setError(null);
        txtNamaPegawai_jv.setError(null);
        txtBagianPekerjaan_jv.setError(null);
        txtTelepon_jv.setError(null);
        txtAlamat_jv.setError(null);
        txtNewPassword_jv.setError(null);
        txtReNewPassword_jv.setError(null);
    }

    public void save_register(){
        String email_pegawai = txtEmail_jv.getText().toString().trim();
        String nama_pegawai = txtNamaPegawai_jv.getText().toString().trim();
        String bagian_pegawai = txtBagianPekerjaan_jv.getText().toString().trim();
        String telepon_pegawai = txtTelepon_jv.getText().toString().trim();
        String alamat_pegawai = txtAlamat_jv.getText().toString().trim();
        String password_pegawai = txtNewPassword_jv.getText().toString().trim();

        SaveRegister(email_pegawai, nama_pegawai, bagian_pegawai, telepon_pegawai,alamat_pegawai, password_pegawai);

    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(Register.this);
        builder.setMessage("Apakah Anda Yakin Menutup Aplikasi Ini?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_login) {
            Intent i = new Intent(Register.this,MainActivity.class);
            startActivity(i);
            finish();
        }
        else if (id == R.id.action_register) {
            Intent i = new Intent(Register.this,Register.class);
            startActivity(i);
            finish();
        }
        else if (id == R.id.action_exit) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent startMain = new Intent(Intent.ACTION_MAIN);
                            startMain.addCategory(Intent.CATEGORY_HOME);
                            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(startMain);
                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(Register.this);
            builder.setMessage("Apakah Anda Yakin Menutup Aplikasi Ini?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void SaveRegister(final String email_pegawai_atp, final String nama_pegawai_atp, final String bagian_pegawai_atp, final String telepon_pegawai_atp, final String alamat_pegawai_atp, final String password_pegawai_atp)
    {
        progressDialog.show();
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Deklarasi_Variabel_ATP> call = apiInterface.insertVcard(email_pegawai_atp, nama_pegawai_atp, bagian_pegawai_atp, telepon_pegawai_atp, alamat_pegawai_atp, password_pegawai_atp);

        call.enqueue(new Callback<Deklarasi_Variabel_ATP>() {
            @Override
            public void onResponse(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Response<Deklarasi_Variabel_ATP> response) {
                progressDialog.dismiss();
                if(response.isSuccessful() && response.body() != null)
                {
                    Boolean success = response.body().getSuccess_atp();
                    if (success)
                    {
                        Toast.makeText(Register.this,response.body().getMessage_atp(),Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(Register.this,response.body().getMessage_atp(),Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Register.this,t.getLocalizedMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }
}
