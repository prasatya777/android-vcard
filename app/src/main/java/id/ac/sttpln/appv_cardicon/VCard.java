package id.ac.sttpln.appv_cardicon;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.glxn.qrgen.android.QRCode;
import net.glxn.qrgen.core.image.ImageType;

import java.io.ByteArrayOutputStream;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VCard extends AppCompatActivity {

    TextView TextNama_jv, TextJabatan_jv, TextAlamat_jv, TextTelepon_jv, TextEmail_jv;
    ImageView QRCode_Pegawai_jv;
    String TextNama_st, TextJabatan_st, TextAlamat_st, TextTelepon_st, TextEmail_st;
    String email_to_all;
    ProgressDialog progressDialog;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vcard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu......");

        Bundle email_r = getIntent().getExtras();
        email_to_all = email_r.getString("email");

//      TextView
        TextNama_jv = (TextView)findViewById(R.id.TextNama);
        TextJabatan_jv = (TextView)findViewById(R.id.TextJabatan);
        TextTelepon_jv = (TextView)findViewById(R.id.TextTelepon);
        TextAlamat_jv = (TextView)findViewById(R.id.TextAlamat);
        TextEmail_jv = (TextView)findViewById(R.id.TextEmail);

//      Menampilkan data dari database
        set_read_vcard();

    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(VCard.this);
        builder.setMessage("Apakah Anda Yakin Menutup Aplikasi Ini?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_vcard) {
            Intent i = new Intent(VCard.this,VCard.class);

            Bundle email_s = new Bundle();
            email_s.putString("email",email_to_all);
            i.putExtras(email_s);

            startActivity(i);
            finish();
        }
        else if (id == R.id.action_biodata) {
            Intent i = new Intent(VCard.this,Biodata.class);

            Bundle email_s = new Bundle();
            email_s.putString("email",email_to_all);
            i.putExtras(email_s);

            startActivity(i);
            finish();
        }
        else if (id == R.id.action_account_manager) {
            Intent i = new Intent(VCard.this,AccountManager.class);

            Bundle email_s = new Bundle();
            email_s.putString("email",email_to_all);
            i.putExtras(email_s);

            startActivity(i);
            finish();
        }
        else if (id == R.id.action_logout) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            Toast.makeText(getApplicationContext(),"Logout Sukses",Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(VCard.this,MainActivity.class);
                            startActivity(i);
                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(VCard.this);
            builder.setMessage("Apakah Anda Yakin Keluar?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    public void set_read_vcard(){
        read_vcard(email_to_all);
    }

    private void read_vcard(final String email_pegawai_atp){
        progressDialog.show();
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Deklarasi_Variabel_ATP> call = apiInterface.viewVcard(email_pegawai_atp);
        call.enqueue(new Callback<Deklarasi_Variabel_ATP>() {
            @Override
            public void onResponse(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Response<Deklarasi_Variabel_ATP> response) {
                progressDialog.dismiss();
                if(response.isSuccessful() && response.body() != null)
                {
                    Boolean success = response.body().getSuccess_atp();
                    if (success)
                    {
                        TextNama_st = response.body().getNama_pegawai_atp();
                        TextJabatan_st = response.body().getBagian_pegawai_atp();
                        TextAlamat_st = response.body().getAlamat_pegawai_atp();
                        TextTelepon_st = response.body().getTelepon_pegawai_atp();
                        TextEmail_st = response.body().getEmail_pegawai_atp();

                        TextNama_jv.setText(TextNama_st);
                        TextJabatan_jv.setText(TextJabatan_st);
                        TextAlamat_jv.setText(TextAlamat_st);
                        TextTelepon_jv.setText(TextTelepon_st);
                        TextEmail_jv.setText(TextEmail_st);

                //      QRQODE Generator
                        QRCode_Pegawai_jv = (ImageView) findViewById(R.id.QRCode_Pegawai);
                        net.glxn.qrgen.core.scheme.VCard vCard = new net.glxn.qrgen.core.scheme.VCard(TextNama_st)
                                .setAddress(TextAlamat_st)
                                .setCompany("ICON +")
                                .setTitle(TextJabatan_st)
                                .setPhoneNumber(TextTelepon_st)
                                .setEmail(TextEmail_st);

                        ByteArrayOutputStream bout =
                                QRCode.from(vCard)
                                        .withSize(500, 500)
                                        .to(ImageType.PNG)
                                        .stream();
                        byte[] bitmapdata = bout.toByteArray();
                        Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);

                        Bitmap imageRounded = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
                        Canvas canvas = new Canvas(imageRounded);
                        Paint mpaint = new Paint();
                        mpaint.setAntiAlias(true);
                        mpaint.setShader(new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
                        canvas.drawRoundRect((new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight())), 20, 20, mpaint);// Round Image Corner 100 100 100 100

                        QRCode_Pegawai_jv.setImageBitmap(imageRounded);
                    }
                    else
                    {
                        Toast.makeText(VCard.this,"Error !",Toast.LENGTH_LONG).show();
                        Intent i = new Intent(VCard.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(VCard.this,t.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                Intent i = new Intent(VCard.this,MainActivity.class);
                startActivity(i);
                finish();

            }
        });
    }

}
