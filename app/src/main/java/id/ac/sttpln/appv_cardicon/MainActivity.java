package id.ac.sttpln.appv_cardicon;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    EditText txtUsername_jv, txtPassword_jv;
    Button btnLogin_jv;
    String txtUsername_validasi, txtPassword_validasi;
    ProgressDialog progressDialog;
    ApiInterface apiInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtUsername_jv = (EditText)findViewById(R.id.txtUsername);
        txtPassword_jv = (EditText)findViewById(R.id.txtPassword);
        btnLogin_jv = (Button)findViewById(R.id.btnLogin);

        btnLogin_jv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validasi_login();
            }
        });
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu......");
    }

    public void validasi_login(){
        clear_seterror();
        txtUsername_validasi = txtUsername_jv.getText().toString();
        txtPassword_validasi = txtPassword_jv.getText().toString();

        if (TextUtils.isEmpty(txtUsername_validasi))
        {
            txtUsername_jv.setError("Username Tidak Boleh Kosong");
            txtUsername_jv.requestFocus();
        }
        else if (TextUtils.isEmpty(txtPassword_validasi))
        {
            txtPassword_jv.setError("Password Tidak Boleh Kosong");
            txtPassword_jv.requestFocus();
        }
        else
        {
            enter_login();
        }
    }

    public void enter_login() {
        String email_pegawai = txtUsername_jv.getText().toString().trim();
        String password_pegawai = txtPassword_jv.getText().toString().trim();
        ValLogin(email_pegawai, password_pegawai);
    }

    public void clear_seterror()
    {
        txtUsername_jv.setError(null);
        txtPassword_jv.setError(null);
    }


    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Apakah Anda Yakin Menutup Aplikasi Ini?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_login) {
            Intent i = new Intent(MainActivity.this,MainActivity.class);
            startActivity(i);
            finish();
        }
        else if (id == R.id.action_register) {
            Intent i = new Intent(MainActivity.this,Register.class);
            startActivity(i);
            finish();
        }
        else if (id == R.id.action_exit) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent startMain = new Intent(Intent.ACTION_MAIN);
                            startMain.addCategory(Intent.CATEGORY_HOME);
                            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(startMain);
                            finish();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Apakah Anda Yakin Menutup Aplikasi Ini?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void ValLogin(final String email_pegawai_atp, String password_pegawai_atp)
    {
        progressDialog.show();
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Deklarasi_Variabel_ATP> call = apiInterface.valloginVcard(email_pegawai_atp, password_pegawai_atp);

        call.enqueue(new Callback<Deklarasi_Variabel_ATP>() {
            @Override
            public void onResponse(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Response<Deklarasi_Variabel_ATP> response) {
                progressDialog.dismiss();
                if(response.isSuccessful() && response.body() != null)
                {
                    Boolean success = response.body().getSuccess_atp();
                    if (success)
                    {
                        Toast.makeText(MainActivity.this,response.body().getMessage_atp(),Toast.LENGTH_LONG).show();
                        txtUsername_jv.setText("");
                        txtPassword_jv.setText("");
                        Intent i = new Intent(MainActivity.this,VCard.class);

                        Bundle login_t_vcard = new Bundle();
                        login_t_vcard.putString("email",response.body().getS_email_pegawai_atp());
                        i.putExtras(login_t_vcard);

                        startActivity(i);
                        finish();
                    }
                    else
                    {
                        AlertDialog.Builder builder= new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage(response.body().getMessage_atp())
                                .setNegativeButton("Retry",null).create().show();
                        txtUsername_jv.setText("");
                        txtPassword_jv.setText("");
                        txtUsername_jv.requestFocus();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this,t.getLocalizedMessage(),Toast.LENGTH_LONG).show();

            }
        });
    }
}
