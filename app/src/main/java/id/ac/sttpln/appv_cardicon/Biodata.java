package id.ac.sttpln.appv_cardicon;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Biodata extends AppCompatActivity {

    EditText txtEmail_jv, txtNamaPegawai_jv, txtBagianPekerjaan_jv, txtTelepon_jv, txtAlamat_jv;
    Button btnUpdate_jv;
    String txtEmail_validasi, txtNamaPegawai_validasi, txtBagianPekerjaan_validasi, txtTelepon_validasi, txtAlamat_validasi;

    String email_to_all;
    ProgressDialog progressDialog;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biodata);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtEmail_jv = (EditText)findViewById(R.id.txtEmail);
        txtNamaPegawai_jv = (EditText)findViewById(R.id.txtNamaPegawai);
        txtBagianPekerjaan_jv = (EditText)findViewById(R.id.txtBagianPekerjaan);
        txtTelepon_jv = (EditText)findViewById(R.id.txtTelepon);
        txtAlamat_jv = (EditText)findViewById(R.id.txtAlamat);
        btnUpdate_jv = (Button)findViewById(R.id.btnUpdate);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu......");

        Bundle email_r = getIntent().getExtras();
        email_to_all = email_r.getString("email");

        set_read_vcard();

//      Scroll Text Alamat dalam Scroll View
        txtAlamat_jv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (txtAlamat_jv.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });

//       Buton Update
        btnUpdate_jv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear_seterror();
                txtEmail_validasi = txtEmail_jv.getText().toString();
                if (TextUtils.isEmpty(txtEmail_validasi))
                {
                    txtEmail_jv.setError("Maaf Email Tidak Boleh Kosong");
                    txtEmail_jv.requestFocus();
                }
                else if (isValidEmail(txtEmail_jv.getText().toString()))
                {
                    validasi_biodata();
                } else {
                    txtEmail_jv.setError("Format Email Salah");
                    txtEmail_jv.requestFocus();
                }

            }
        });

    }

    public static boolean isValidEmail(String email) {
        boolean validate;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String emailPattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";

        if (email.matches(emailPattern)) {
            validate = true;
        } else if (email.matches(emailPattern2)) {
            validate = true;
        } else {
            validate = false;
        }

        return validate;
    }

    public void validasi_biodata(){
        txtNamaPegawai_validasi = txtNamaPegawai_jv.getText().toString();
        txtBagianPekerjaan_validasi = txtBagianPekerjaan_jv.getText().toString();
        txtTelepon_validasi = txtTelepon_jv.getText().toString();
        txtAlamat_validasi = txtAlamat_jv.getText().toString();


        if (TextUtils.isEmpty(txtNamaPegawai_validasi))
        {
            txtNamaPegawai_jv.setError("Maaf Nama Pegawai Tidak Boleh Kosong");
            txtNamaPegawai_jv.requestFocus();
        }
        else if (TextUtils.isEmpty(txtBagianPekerjaan_validasi))
        {
            txtBagianPekerjaan_jv.setError("Maaf Bagian Pekerjaan Tidak Boleh Kosong");
            txtBagianPekerjaan_jv.requestFocus();
        }
        else if (TextUtils.isEmpty(txtTelepon_validasi))
        {
            txtTelepon_jv.setError("Maaf Telepon Tidak Boleh Kosong");
            txtTelepon_jv.requestFocus();
        }
        else if (TextUtils.isEmpty(txtAlamat_validasi))
        {
            txtAlamat_jv.setError("Maaf Alamat Tidak Boleh Kosong");
            txtAlamat_jv.requestFocus();
        }
        else
        {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            set_update_Vcard();
                            txtEmail_jv.requestFocus();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(Biodata.this);
            builder.setMessage("Apakah Anda Yakin Update Data Pegawai?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
    }

    public void clear_seterror()
    {
        txtEmail_jv.setError(null);
        txtNamaPegawai_jv.setError(null);
        txtNamaPegawai_jv.setError(null);
        txtBagianPekerjaan_jv.setError(null);
        txtTelepon_jv.setError(null);
        txtAlamat_jv.setError(null);
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(Biodata.this);
        builder.setMessage("Apakah Anda Yakin Menutup Aplikasi Ini?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_vcard) {
            Intent i = new Intent(Biodata.this,VCard.class);

            Bundle email_s = new Bundle();
            email_s.putString("email",email_to_all);
            i.putExtras(email_s);

            startActivity(i);
            finish();
        }
        else if (id == R.id.action_biodata) {
            Intent i = new Intent(Biodata.this,Biodata.class);

            Bundle email_s = new Bundle();
            email_s.putString("email",email_to_all);
            i.putExtras(email_s);

            startActivity(i);
            finish();
        }
        else if (id == R.id.action_account_manager) {
            Intent i = new Intent(Biodata.this,AccountManager.class);

            Bundle email_s = new Bundle();
            email_s.putString("email",email_to_all);
            i.putExtras(email_s);

            startActivity(i);
            finish();
        }
        else if (id == R.id.action_logout) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            Toast.makeText(getApplicationContext(),"Logout Sukses",Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(Biodata.this,MainActivity.class);
                            startActivity(i);
                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(Biodata.this);
            builder.setMessage("Apakah Anda Yakin Keluar?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }
// Tampilkan Data
    public void set_read_vcard(){
        read_vcard(email_to_all);
    }

    public void read_vcard(final String email_pegawai_atp){
        progressDialog.show();
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Deklarasi_Variabel_ATP> call = apiInterface.viewVcard(email_pegawai_atp);
        call.enqueue(new Callback<Deklarasi_Variabel_ATP>() {
            @Override
            public void onResponse(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Response<Deklarasi_Variabel_ATP> response) {
                progressDialog.dismiss();
                if(response.isSuccessful() && response.body() != null)
                {
                    Boolean success = response.body().getSuccess_atp();
                    if (success)
                    {
                        txtEmail_jv.setText(email_to_all);
                        txtNamaPegawai_jv.setText(response.body().getNama_pegawai_atp());
                        txtBagianPekerjaan_jv.setText(response.body().getBagian_pegawai_atp());
                        txtAlamat_jv.setText(response.body().getAlamat_pegawai_atp());
                        txtTelepon_jv.setText(response.body().getTelepon_pegawai_atp());
                    }
                    else
                    {
                        Toast.makeText(Biodata.this,"Error !",Toast.LENGTH_LONG).show();
                        Intent i = new Intent(Biodata.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Biodata.this,t.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                Intent i = new Intent(Biodata.this,MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

//  Update Data
    public void set_update_Vcard(){
        String email_pegawai = txtEmail_jv.getText().toString().trim();
        String nama_pegawai = txtNamaPegawai_jv.getText().toString().trim();
        String bagian_pegawai = txtBagianPekerjaan_jv.getText().toString().trim();
        String telepon_pegawai = txtTelepon_jv.getText().toString().trim();
        String alamat_pegawai = txtAlamat_jv.getText().toString().trim();

        update_Vcard(email_to_all, email_pegawai, nama_pegawai, bagian_pegawai, telepon_pegawai, alamat_pegawai);
    }

    public void update_Vcard(final String s_email_pegawai_atp, final String email_pegawai_atp, final String nama_pegawai_atp, final String bagian_pegawai_atp, final String telepon_pegawai_atp, final String alamat_pegawai_atp){
        progressDialog.show();
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Deklarasi_Variabel_ATP> call = apiInterface.updatebiodataVcard(s_email_pegawai_atp, email_pegawai_atp, nama_pegawai_atp, bagian_pegawai_atp, telepon_pegawai_atp, alamat_pegawai_atp);
        call.enqueue(new Callback<Deklarasi_Variabel_ATP>() {
            @Override
            public void onResponse(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Response<Deklarasi_Variabel_ATP> response) {
                progressDialog.dismiss();
                if(response.isSuccessful() && response.body() != null)
                {
                    Boolean success = response.body().getSuccess_atp();
                    if (success)
                    {
                        Toast.makeText(Biodata.this,response.body().getMessage_atp(),Toast.LENGTH_LONG).show();
                        email_to_all = response.body().getS_email_pegawai_atp();
                    }
                    else
                    {
                        Toast.makeText(Biodata.this,response.body().getMessage_atp(),Toast.LENGTH_LONG).show();
                        Intent i = new Intent(Biodata.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Deklarasi_Variabel_ATP> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Biodata.this,t.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                Intent i = new Intent(Biodata.this,MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}
